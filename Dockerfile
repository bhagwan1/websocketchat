# For Java 8, try this
FROM tomcat:9.0-slim

# Refer to Maven build -> finalName
ARG WAR_FILE=target/websocket.war

# mkdir cd /usr/local/tomcat/webapps
WORKDIR /usr/local/tomcat/webapps

# cp target/websocket.war /usr/local/tomcat/webapps/websocket.war
COPY ${WAR_FILE} websocket.war

# java -jar /opt/app/app.jar
ENTRYPOINT ["catalina.sh", "run"]